
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { Daftar } from '../pages/daftar/daftar';
import { TabsPage } from '../pages/tab/tabs';
import { Tab } from '../pages/tab1/tab';
import { Tab2 } from '../pages/tab2/tab2';
import { Menu1 } from '../pages/menu1/menu1';
import { Menu2 } from '../pages/menu2/menu2';
import { Antri1 } from '../pages/antri1/antri1';
import { Antri2 } from '../pages/antri2/antri2';



@NgModule({
  declarations: [
  MyApp,
  HomePage,
  Daftar,
  TabsPage,
  Tab,
  Tab2,
  Menu1,
  Menu2,
  Antri1,
  Antri2
  ],
  imports: [
  BrowserModule,
  IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
  MyApp,
  HomePage,
  Daftar,
  TabsPage,
  Tab,
  Tab2,
  Menu1,
  Menu2,
  Antri1,
  Antri2
  ],
  providers: [
  StatusBar,
  SplashScreen,
  {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
