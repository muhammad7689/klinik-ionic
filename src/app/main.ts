import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { HomePage } from '../pages/home/home';
import { AppModule } from './app.module';

platformBrowserDynamic().bootstrapModule(AppModule);