import { Component } from '@angular/core';
import { NavController, App } from 'ionic-angular';
import { Menu1 } from '../menu1/menu1';
import { Menu2 } from '../menu2/menu2';

@Component({
	selector : 'page-tab',
	templateUrl: 'tab.html'
})
export class Tab {

	constructor(public navCtrl: NavController, public app : App) {
        
  }

	menu1(): void{
  	this.app.getRootNav().push(Menu1)
}
  	menu2(): void{
  	this.app.getRootNav().push(Menu2)
}
}