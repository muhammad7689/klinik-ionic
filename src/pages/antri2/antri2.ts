import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { TabsPage } from '../tab/tabs';

@Component({
  templateUrl: 'antri2.html'
})
export class Antri2 {

  constructor(public navCtrl: NavController) {
  
  }
    openPage(): void{
  	this.navCtrl.setRoot(TabsPage);
  }
}
