import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Menu2 } from '../menu2/menu2';

@Component({
	selector: 'page-tabs2',
	templateUrl: 'tab2.html'
})
export class Tab2 {

  constructor(public navCtrl: NavController) {
        
  }

}
