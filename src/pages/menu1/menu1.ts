import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Antri1 } from '../antri1/antri1';

@Component({
  templateUrl: 'menu1.html'
})
export class Menu1 {

  constructor(public navCtrl: NavController) {
  
  }
  push(): void{
  	this.navCtrl.setRoot(Antri1, {},{
  	  animate: true,
      animation: 'md-transition'
    });
  }
}
