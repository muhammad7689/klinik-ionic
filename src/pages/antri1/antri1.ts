import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { TabsPage } from '../tab/tabs';

@Component({
  templateUrl: 'antri1.html'
})
export class Antri1 {

  constructor(public navCtrl: NavController) {
  
  }
  openPage(): void{
  	this.navCtrl.setRoot(TabsPage);
  }
}
