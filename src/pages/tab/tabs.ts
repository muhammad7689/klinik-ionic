import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { Tab } from '../tab1/tab';
import { Tab2 } from '../tab2/tab2';


@Component({

	templateUrl: 'tabs.html'
})	
export class TabsPage {

	tab1Root = Tab;
	tab2Root = Tab2;

	constructor(public navCtrl: NavController) {

	}

	back(): void{
		this.navCtrl.setRoot(HomePage, {}, {
			animate: true,
			animation: 'md-transition'
		});
	}

}

