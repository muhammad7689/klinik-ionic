import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Daftar } from '../daftar/daftar';
import { TabsPage } from '../tab/tabs';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {
  
  }
  
  openPage(): void{
  	this.navCtrl.push(Daftar, {},{
      animation: 'md-transition'
    });
  }

  open(): void{
  	this.navCtrl.push(TabsPage, {}, {
      animation: 'wp-transation'
    });
  }

}
