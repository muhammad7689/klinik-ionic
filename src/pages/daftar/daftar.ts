import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { HomePage } from '../home/home';

@Component({
  selector: 'page-daftar',
  templateUrl: 'daftar.html'
})
export class Daftar {

  constructor(public navCtrl: NavController) {
  
  }
openPage(): void{
  	this.navCtrl.setRoot(HomePage);
  }
}
