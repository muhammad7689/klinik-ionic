import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Antri2 } from '../antri2/antri2';

@Component({
	templateUrl: 'menu2.html'
})
export class Menu2 {

	constructor(public navCtrl: NavController) {
		
	}
	push(): void{
		this.navCtrl.setRoot(Antri2, {},{
			animate: true,
			animation: 'md-transition'
		});
	}
}
